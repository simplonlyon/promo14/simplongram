# Projet Express Typescript Typeorm



## How To Use
1. Run `npm i` command
2. Create a database
3. Modify or create DATABASE_URL in .env
4. Add JWT_SECRET to .env
5. Add SERVER_URL to .env and put machine ip (`ifconfig` or `ipconfig` in shell, example : `http://10.0.21.194:8000`) 
6. Run `npm start` command

## Charger les fixtures
Exécuter npx ts-node bin/fixtures.ts pour charger les données de test dans votre base de donnée de dév (renvoie également un token de test infini)

## Thunder Client
Importer le fichier thunder-collection_api-test.json dans une collection Thunder Client pour avoir des requêtes de test à exécuter sous la main
