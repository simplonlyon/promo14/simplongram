import 'dotenv-flow/config';
import { createConnection, getConnection } from 'typeorm';
import { generateToken } from '../src/utils/token';
import { userFixtures, pictureFixtures } from '../test/setUp';

async function doFixture() {

    await createConnection({
        type: 'mysql',
        url: process.env.DATABASE_URL,
        synchronize: true,
        entities: ['src/entity/*.ts']
    });
    await getConnection().synchronize(true);
    

    await userFixtures();
    await pictureFixtures();

    console.log(generateToken({id:'test-user'}, 99999));

    await getConnection().close();
}

doFixture();

