import { User as MyUser } from "./src/entity/User";

declare global {
    namespace Express {
      interface User extends MyUser {}
    }
  }