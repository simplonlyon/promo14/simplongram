import { Router } from "express";

import { getRepository } from "typeorm";
import { Picture } from "../entity/Picture";

export const accountController = Router();

accountController.get('/', (req, res) => {
    res.json(req.user);
});

accountController.get('/pictures', async (req, res) => {
    try {
        const pictures = await getRepository(Picture).find({ author: req.user });
        res.json(pictures);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});

