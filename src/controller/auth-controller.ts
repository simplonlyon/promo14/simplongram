import { Router } from "express";
import Joi from "joi";
import { getRepository } from "typeorm";
import { User } from "../entity/User";
import { uploadImage } from "../utils/uploader";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";

export const authController = Router();

const userValidation = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().min(4).required(),
    avatar: Joi.string().base64().required()
})

authController.post('/', async (req, res) => {
    try {
        const {error} = userValidation.validate(req.body);
        if(error) {
            res.status(400).json({errors: error.details.map(err => err.message)});
            return;
        }
        if(await getRepository(User).findOne({username: req.body.username})) {
            res.status(400).json({errors: ['User already exists']});
            return;
        }
        const avatarUrl = await uploadImage(req.body.avatar);
        const user = new User();
        Object.assign(user, req.body);
        user.avatar = avatarUrl;
        user.password = await bcrypt.hash(user.password, 11);
        await getRepository(User).save(user);
        res.status(201).json({
            user,
            token: generateToken({id:user.id})
        });
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});

authController.post('/login', async (req,res) => {
    try {
        const user = await getRepository(User).findOne({username: req.body.username});
        if(!user || !(await bcrypt.compare(req.body.password, user.password))) {
            res.status(401).end();
            return;
        }
        res.json({
            user,
            token: generateToken({id:user.id})
        })
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});