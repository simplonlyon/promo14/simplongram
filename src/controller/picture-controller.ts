import { Router } from "express";
import Joi from "joi";
import passport from "passport";
import { getRepository } from "typeorm";
import { Picture } from "../entity/Picture";
import { uploadImage } from "../utils/uploader";


const pictureValidation = Joi.object({
    description: Joi.string().required(),
    file: Joi.string().base64().required()
})

export const pictureController = Router();

pictureController.get('/', async (req, res) => {
    const page = Number(req.query.page)?Number(req.query.page):1;
    try {
        const pictures = await getRepository(Picture).find({take: 3, skip: (page-1)*3, order: {postDate:'DESC'}});
        res.json(pictures);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});


pictureController.post('/', passport.authenticate('jwt', {session:false}), async (req, res) => {
    try {
        const {error} = pictureValidation.validate(req.body);
        if(error) {
            console.log(error);
            res.status(400).json({errors: error.details.map(err => err.message)});
            return;
        }
        console.log('from picture controller')

        const filename = await uploadImage(req.body.file);
        const picture = new Picture()

        picture.author = req.user;
        picture.description = req.body.description;
        picture.filename = filename;
    picture.postDate = new Date();

        await getRepository(Picture).save(picture);

        res.status(201).json(picture);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});

pictureController.patch('/:id/like', passport.authenticate('jwt', {session:false}), async (req, res) => {
    try {
        const picture = await getRepository(Picture).findOne(req.params.id, {relations: ['likes']});
        if(!picture){
            res.status(404).end();
            return;
        }

        const found = picture.likes.findIndex(item => item.id === req.user.id);
 
        if(found !== -1){
            picture.likes.splice(found, 1)
        } else {
            picture.likes.push(req.user);
        }

        await getRepository(Picture).save(picture);
        res.status(200).json(picture);
    } catch (error) {
        console.log(error);
        res.status(500).end();
    }
});