import { Column, Entity, ManyToMany, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { User } from "./User";


@Entity()
export class Picture {
    @PrimaryGeneratedColumn('uuid')
    id:string;
    @Column()
    filename:string;
    @Column('text')
    description:string;
    @Column('datetime')
    postDate:Date;

    @ManyToOne(() => User, user => user.pictures, {eager:true})
    author:User;

    @ManyToMany(() => User, user => user.favorites)
    likes:User[];

    toJSON() {
        return {
            ...this,
            imageUrl: this.filename.startsWith('http')?this.filename:process.env.SERVER_URL+'/uploads/'+this.filename,
            thumbnailUrl: this.filename.startsWith('http')?this.filename:process.env.SERVER_URL+'/uploads/thumbnails/'+this.filename,
        }

    }
    
}

