import { Column, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Picture } from "./Picture";

@Entity()
export class User {

    @PrimaryGeneratedColumn('uuid')
    id:string;
    @Column({unique:true, length: 180})
    username:string;
    @Column()
    password:string;
    @Column()
    avatar:string;

    @OneToMany(() => Picture, picture => picture.author)
    pictures:Picture[]

    @ManyToMany(() => Picture, picture => picture.likes)
    @JoinTable()
    favorites:Picture[];

    toJSON() {
        return {
            ...this,
            password: undefined
        }
    }
}