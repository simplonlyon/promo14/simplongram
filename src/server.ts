import express from 'express';
import cors from 'cors';
import { configurePassport } from './utils/token';
import passport from 'passport';
import { pictureController } from './controller/picture-controller';
import { authController } from './controller/auth-controller';
import { accountController } from './controller/account-controller';

export const server = express();

configurePassport();
server.use(express.json({limit:'10mb'}));
server.use(cors());
server.use(passport.initialize());
server.use(express.static('public'));

server.use('/api/picture', pictureController);
server.use('/api/user', authController);
server.use('/api/account', passport.authenticate('jwt', {session: false}), accountController);

// server.use('/api/machin', machinController)