import sharp from "sharp";
import { randomUUID } from 'crypto';

export async function uploadImage(base64: string) {

    const filename = randomUUID() + '.jpg';
    const buffer = Buffer.from(base64, 'base64');

    const img = sharp(buffer)
        .jpeg({ quality: 70 });
    await Promise.all([
        img.toFile(__dirname + '/../../public/uploads/' + filename),
        img.resize(200, 200).toFile(__dirname + '/../../public/uploads/thumbnails/' + filename)
    ]);

    return filename;
}