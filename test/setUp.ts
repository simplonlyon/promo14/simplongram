import { createConnection, getManager, getConnection } from "typeorm";
import { User } from "../src/entity/User";
import bcrypt from 'bcrypt';
import { Picture } from "../src/entity/Picture";

/**
 * Petite fonction qui met en place une base de données de test en mémoire avec
 * sqlite (et plus spécifiquement la library better-sqlite3).
 * Cette db sera recréée avant chaque test et supprimer après chaque test, il faut donc
 * faire également en sorte d'y mettre des données de test si c'est nécessaire (les fonctions
 * de fixtures servent à ça)
 */
export function setUpTestDatabase() {

    beforeEach(async () => {
        await createConnection({
            type: 'better-sqlite3',
            database: ':memory:',
            synchronize: true,
            entities: ['src/entity/*.ts'],
            dropSchema: true
        });
        
    });

    afterEach(async () => {
        await getConnection().close()
    })
}
/**
 * Fonction qui fait persister des personnes pour les tests. Il faudra la lancer
 * avant chaque test et en créer d'autres pour les autres entités à tester.
 */
export async function userFixtures() {
    const password = await bcrypt.hash('1234', 1);
    await getManager().insert(User, [
        {id: 'test-user', username: 'simplon', password, avatar: 'https://www.simplonlyon.fr/AccueilSimplon/wp-content/uploads/2021/02/avatar.png'}
    ]);
}

export async function pictureFixtures() {
    await getManager().insert(Picture, [
        {id:'test-picture1', author: {id: 'test-user'}, description:'Test picture', filename: 'https://cdn.pixabay.com/photo/2019/05/05/14/02/capybara-4180603_960_720.jpg', postDate: new Date()},
        {id:'test-picture2', author: {id: 'test-user'}, description:'Test picture 2', filename: 'https://cdn.pixabay.com/photo/2017/08/18/06/49/capybara-2653996_960_720.jpg', postDate: new Date()}
    ]);
}